//
//  StorageManager.swift
//  StorageApplication
//
//  Created by Александр Уткин on 27.06.2020.
//  Copyright © 2020 Александр Уткин. All rights reserved.
//

import Foundation
import RealmSwift

let realm = try! Realm()

class StorageManager {
    
    static func save(task: ModelRealm) {
        
        try! realm.write {
            realm.add(task)
        }
    }
}

